/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aridolfi <aridolfi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 14:10:29 by aridolfi          #+#    #+#             */
/*   Updated: 2017/01/16 13:17:27 by aridolfi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <fcntl.h>
#include <locale.h>

int			main(void)
{
	// char	*tmp;
	setlocale(LC_ALL, "en_US");
	int			i;
	int			j;

	i = ft_printf("ft %ls\n", L"ÿ≠ŸM-^E ÿÆŸM-^Fÿ≤ŸM-ÿ±");
	j = printf("pf %ls\n", L"ÿ≠ŸM-^E ÿÆŸM-^Fÿ≤ŸM-ÿ±");
	printf("%d\n", i);
	printf("%d\n", j);

	// setlocale(LC_ALL, "en_GB");
	// i = ft_printf("%.14c\n", 42);
	// j = ft_printf("%05d", 42);
	// printf("%d\n", j - 1);
	// j = printf("%05d", 42);
	// printf("%d\n", i - 1);
	// printf("%d\n", j - 1);
	return (0);
}

// printf("Minus : %c\n", f->minus);
// printf("Plus : %c\n", f->plus);
// printf("Zero : %c\n", f->zero);
// printf("Space : %c\n", f->space);
// printf("hash : %c\n", f->hash);
// printf("len : %d\n", f->len);
// printf("prec : %d\n", f->prec);
// printf("mod : %s\n", f->mod);
// printf("conv : %c\n", f->conv);
