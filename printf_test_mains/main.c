/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aridolfi <aridolfi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 14:10:29 by aridolfi          #+#    #+#             */
/*   Updated: 2017/01/16 14:08:27 by aridolfi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <fcntl.h>
#include <locale.h>

int			main(void)
{
	setlocale(LC_ALL, "en_US");
	int			i;
	// int			j;
	// short		k;
	//
	// k = 0xFFFFU;
	// i = ft_printf("%hD, %hD\n", (short)0, k);
	// j = printf("%hD, %hD\n", (short)0, k);
	i = ft_printf("%d\n", 42);
	// j = printf("%#o\n", 0);
	printf("%d\n", i);
	// printf("%d\n", j);
	return (0);
}

// printf("Minus : %c\n", f->minus);
// printf("Plus : %c\n", f->plus);
// printf("Zero : %c\n", f->zero);
// printf("Space : %c\n", f->space);
// printf("hash : %c\n", f->hash);
// printf("len : %d\n", f->len);
// printf("prec : %d\n", f->prec);
// printf("mod : %s\n", f->mod);
// printf("conv : %c\n", f->conv);
